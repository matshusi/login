package com.example.loginproject.myloginapp.repository;

import com.example.loginproject.myloginapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginDAO extends JpaRepository<User,Long > {

}
