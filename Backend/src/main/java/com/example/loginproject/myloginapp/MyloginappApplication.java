package com.example.loginproject.myloginapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyloginappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyloginappApplication.class, args);
	}

}
