package com.example.loginproject.myloginapp.model;


import javax.persistence.*;


@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;
    private String username;
    private String password;


    protected User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }
@Override
    public String toString(){
        return String.format("LoginModel[id=%d, username='%s', password='%s']",id, username, password);
}

public Long getId(){
        return id;
}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
